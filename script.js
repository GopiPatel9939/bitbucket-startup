
$( document ).ready(function() {
    $('ol').css('width','500px');

    $('.addBtn').click(function() {
        var NewList = $('#new-item').val();

        if(NewList.length!=0){
            if ( $('ol li:contains("'+NewList+'")').length ) {
                alert("This item is already added!");
            }else{
                $("ol").append('<li id="lstItem'+ NewList +'" class="unDoneText"><span>' + NewList + 
                '</span><button class="done'+ NewList +' listOption done" id="'+ NewList +'"> Done </button>'+
                ' <button id="'+NewList+'" class="remove'+ NewList +' listOption remove"> Remove </button>'+
                '</li>');
                $('#new-item').val('');
            }
        }else{
            alert("Please enter valid input");
        }       
    });

    $(document).on("click", ".done", function() {
        var NewList = $(this).attr('id');
        if(
            $('#lstItem'+NewList+' > span').css("text-decoration")=='line-through solid rgb(255, 0, 0)' || 
            $('#lstItem'+NewList+' > span').css("text-decoration")=='line-through solid rgb(0, 128, 0)'
        ){
            $('#lstItem'+NewList+' > span').css("text-decoration", "none");
            $(this).html('done');
            $('#lstItem'+NewList+' > span').removeClass('doneText');
            $('#lstItem'+NewList+' > span').addClass('unDoneText');
        }
        else{
            $('#lstItem'+NewList+' > span').css("text-decoration", "line-through");
            $(this).html('undo');
            $('#lstItem'+NewList+' > span').fadeIn("slow");
            $('#lstItem'+NewList+' > span').removeClass('unDoneText');
            $('#lstItem'+NewList+' > span').addClass('doneText');
        }
    });


    $(document).on("click", ".remove", function() {
        var NewList = $(this).attr('id');
        $(this).fadeOut("slow");
        $("#lstItem" +NewList).fadeOut("slow");
    });

});