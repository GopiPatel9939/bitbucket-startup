## Gopi Patel       ## Student Number: 8759939

**How to use To-do list**

## Add list to To-do:

Follow these steps 

1. Add your list that you need to do in To-do list
2. Click plus button on the right side.
3. It will be adding your list.

---

## Delete a list:

If you delete the list follow this steps

1. If you want to delete specific value you can click Remove button.

---

## Completed task:

If you have completed task folloew these steps

1. Click Done button If your task was done.
2. Click Undo button If you want to add again in your list.

## How to open the Application:

1. Click on the index it will run in the brower.

Why did I choose MIT License?
The MIT license is a great choice because it allows you to share your code under a copyleft license without forcing others to expose their proprietary code, it's business friendly and open source friendly while still allowing for monetization.